﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour {

	private static float g = 1E-2f; // universal gravity constant
	private static float gravityRadiusFactor = 100f;
	private static int layerMask = 1 << LayerMask.NameToLayer("Player");

	void FixedUpdate() {
		float gravityRadius = transform.localScale.x * gravityRadiusFactor;

		Collider[] players = Physics.OverlapSphere(transform.position, gravityRadius, layerMask);

		foreach (Collider playerCollider in players) {
			GameObject player = playerCollider.gameObject;

			Rigidbody playerBody = player.GetComponent<Rigidbody>();

			Vector3 forceDirection = transform.position - player.transform.position; // get the force direction

			float squaredDistance = forceDirection.sqrMagnitude; // get the squared distance

			// calculate the force intensity using Newton's law
			float planetMass = GetComponent<Rigidbody> ().mass;
			float playerMass = playerBody.mass;

			float gForce = g * planetMass * playerMass / squaredDistance;

			Vector3 resForce = gForce * forceDirection.normalized; // accumulate in the resulting force variable

			playerBody.AddForce(resForce); // apply the resulting gravity force
		}
	}
}
