﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private bool jump = false;
	private GameObject currentPlanet;

	private static int planetLayer = LayerMask.NameToLayer("Planet");
	private static int planetMask = 1 << LayerMask.NameToLayer("Planet");
	private static float planetRadius = 10f;

	public float jumpForce = 50000f;
	public float moveForce = 1000f;

	public GameObject representation;
	public GameObject feet;
	public GameObject head;

	void Start () {
	
	}

	void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision" + collision);
		
		GameObject candidate = collision.gameObject;
		
		if (candidate.layer == planetLayer) {
			currentPlanet = candidate;
		}
	}
	
	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision" + collision);
		
		GameObject candidate = collision.gameObject;
		
		if (candidate.layer == planetLayer) {
			currentPlanet = null;
		}
	}
	
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			jump = true;
		}

		Stand ();
	}

	void FixedUpdate() {
		if (currentPlanet != null) {
			if (jump) {
				Jump ();
			}

			Move ();
		}
	}

	private void Jump() {
		jump = false;
		
		Debug.Log("jump");
		
		Vector3 forceDirection = transform.position - currentPlanet.transform.position; // get the force direction
		
		Vector3 resForce = jumpForce * forceDirection.normalized; // accumulate in the resulting force variable
		
		GetComponent<Rigidbody>().AddForce(resForce); // apply the resulting gravity force
	}
	
	private void Stand() {

		Collider[] planets = Physics.OverlapSphere(transform.position, planetRadius, planetMask);

		GameObject bestPlanet = null;

		float closestDistanceSqr = Mathf.Infinity;
		Vector3 currentPosition = transform.position;
		foreach (Collider planetCollider in planets) {
			GameObject potentialPlanet = planetCollider.gameObject;
			Vector3 directionToTarget = potentialPlanet.transform.position - currentPosition;
			float dSqrToTarget = directionToTarget.sqrMagnitude;
			if(dSqrToTarget < closestDistanceSqr)
			{
				closestDistanceSqr = dSqrToTarget;
				bestPlanet = potentialPlanet;
			}
		}

		if (bestPlanet != null) {
			// TODO: Align representation with planet

			Vector3 direction = bestPlanet.transform.position - transform.position;

			representation.transform.rotation = Quaternion.LookRotation(direction);
		}
	}
	
	private void Move() {
		float movement = Input.GetAxis ("Vertical");
		
		Vector3 forceDirection = Quaternion.AngleAxis(-90, Vector3.up) * (transform.position - currentPlanet.transform.position);
		
		Vector3 resForce = movement * moveForce * forceDirection.normalized;
		
		GetComponent<Rigidbody>().AddForce(resForce); // apply the resulting gravity force
	}
}
